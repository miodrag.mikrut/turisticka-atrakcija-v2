package com.miodrag.mikrut.turistickeatrakcije.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.adapters.MojRecyclerViewAdapterListAtrakcije;
import com.miodrag.mikrut.turistickeatrakcije.db.DatabaseHelper;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;
import com.miodrag.mikrut.turistickeatrakcije.dialog.AboutDialog;
import com.miodrag.mikrut.turistickeatrakcije.tools.Tools;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Detail extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    List<String> drawerItems;
    ListView drawerList;
    RecyclerView recyclerView;
    MojRecyclerViewAdapterListAtrakcije mojRecyclerViewAdapterListAtrakcije;

    private TextView naziv;
    private TextView opis;
    private TextView telefon;
    private TextView cena;
    private TextView web_adresa;
    private TextView adresa;
    private TextView radno_vreme;
    private ImageButton imageButton;
    private Button btn_add;
    private Button btn_cancel;
    //    private Slika slika;
    private Atrakcija atrakcija;
    private int objekat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        fillData();
        setupToolbar();
        setupDrawer();
        inicijalizacija();
        preuzmiPodatkeIzIntenta();
        fullizeImage();
    }



    private void inicijalizacija() {
        naziv = findViewById(R.id.detail_naziv);
        opis = findViewById(R.id.detail_opis);
        radno_vreme = findViewById(R.id.detail_radno_vreme);
        adresa = findViewById(R.id.detail_adresa);
        web_adresa = findViewById(R.id.detail_web_adresa);
        telefon = findViewById(R.id.detail_telefon);
        imageButton = findViewById(R.id.detail_image_button);
        cena = findViewById(R.id.detail_cena);
    }

    //preuzima podatke koje se nalaze u intentu koji je trigerovao aktivnost
    //setuje vrednosti polja na trenutnu vrednost objekta izvucenog iz baze na osnovu objekat_id
    private void preuzmiPodatkeIzIntenta() {
        objekat_id = getIntent().getIntExtra("objekat_id", -1);
        if (objekat_id < 0) {
            Toast.makeText(this, "Greska! " + objekat_id + " ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            atrakcija = getDatabaseHelper().getAtrakcijaDao().queryForId(objekat_id);
//            slike = getDatabaseHelper().getSlikaDao().queryForEq("nekretnina_id", objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        naziv.setText(atrakcija.getNaziv());
        opis.setText(atrakcija.getOpis());
        telefon.setText(atrakcija.getTelefon());
        adresa.setText(atrakcija.getAdresa());
        cena.setText(atrakcija.getCena());
        web_adresa.setText(atrakcija.getWeb_adresa());
        radno_vreme.setText(atrakcija.getRadno_vreme());
        imageButton.setImageBitmap(BitmapFactory.decodeFile(atrakcija.getImage_path()));
//        Toast.makeText(this, " "+atrakcija.getImage_path(), Toast.LENGTH_SHORT).show();

        telefon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.dialPhoneNumber(atrakcija.getTelefon(), Detail.this);
            }
        });

        web_adresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.openWebPage(web_adresa.getText().toString().trim(),Detail.this);
            }
        });

        adresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Tools.openGoogleMap(Detail.this,atrakcija.getAdresa());
            }
        });

    }

    private void fullizeImage() {
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Detail.this,ZoomImage.class);
                intent.putExtra("objekat_id",atrakcija.getId());
                startActivity(intent);
            }
        });
    }

    private void fillData(){
        drawerItems = new ArrayList<>();
        drawerItems.add("Atrakcije");
        drawerItems.add("Settings");
        drawerItems.add("About the app");
    }

    //podesavanje Drawera
    private void setupDrawer(){
        drawerList = findViewById(R.id.left_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, drawerItems));
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = "Unknown";
                switch (i){
                    case 0:
                        title = "Atrakcije";
                        startActivity(new Intent(Detail.this, Main.class));
                        break;
                    case 1:
                        Toast.makeText(getBaseContext(),"Prikaz settings-a",Toast.LENGTH_SHORT);
                        title = "Settings";
                        Intent intent1 = new Intent(Detail.this, SettingsActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        AboutDialog dialog = new AboutDialog(Detail.this);
                        dialog.show();
                        title = "About the app";
                        break;
                    default:
                        break;
                }
                //drawerList.setItemChecked(i, true);
                setTitle(title);
                drawerLayout.closeDrawer(drawerList);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawerLayout,                   /* DrawerLayout object */
                toolbar,                        /* nav drawer image to replace 'Up' caret */
                R.string.app_name,           /* "open drawer" description for accessibility */
                R.string.app_name           /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }
        };
    }

    //Pocetak setup-a za toolbar
    // action bar prikazuje opcije iz meni.xml
    //uneti u action main.xml AppBarLayout i onda Toolbar

    //setuje toolbar
    private void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_hamburger_white);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_detail, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, Main.class));
                break;
            case R.id.action_edit:
                Intent intent1 = new Intent(Detail.this, Edit.class);
                intent1.putExtra("objekat_id", atrakcija.getId());
                startActivity(intent1);
                break;
            case R.id.action_delete:
                delete();

                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //^^^ kraj setupa za toolbar



    //brise objekat i sve objekte koji su vezani
    //izbacuje alert dialog za potvrdu brisanja
    //prikazuje notifikacije ako je odabrana ta opcija u Settings
    private void delete() {
        AlertDialog dialogDelete = new AlertDialog.Builder(this)
                .setTitle("Delete")
                .setMessage("Are you sure you want to permanently delete?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        try {
//                            List<Slika> slike = getDatabaseHelper().getSlikaDao().queryForEq("nekretnina_id", nekretnina.getId());

                            getDatabaseHelper().getAtrakcijaDao().delete(atrakcija);
//                            for (Slika slika : slike) {
//                                getDatabaseHelper().getSlikaDao().delete(slika);
//                            }
                        } catch (SQLException e) {
                            e.printStackTrace();
                        }
                        if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                            Toast.makeText(Detail.this, atrakcija + " deleted.", Toast.LENGTH_SHORT).show();
                        }
                        if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                            Tools.createNotification(Detail.this, atrakcija, "delete");
                        }
                        Intent intent = new Intent(Detail.this, Main.class);
                        startActivity(intent);
                        if (Tools.proveraPrefsPodesavanja("toast", Detail.this)) {
                            Toast.makeText(Detail.this, atrakcija + " deleted", Toast.LENGTH_SHORT).show();
                        }
                        if (Tools.proveraPrefsPodesavanja("notifications", Detail.this)) {
                            Tools.createNotification(Detail.this, atrakcija, "delete");
                        }
                    }
                })
                .setNegativeButton("Cancel", null)
                .show();
    }


    //***** za komuikaciju sa bazom i oslobadjanje resursa
    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
    //***** KRAJ za komuikaciju sa bazom i oslobadjanje resursa



}
