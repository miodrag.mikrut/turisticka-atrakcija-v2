package com.miodrag.mikrut.turistickeatrakcije.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;

import java.util.List;

public class MojRecyclerViewAdapterListAtrakcije extends
        RecyclerView.Adapter<MojRecyclerViewAdapterListAtrakcije.MyViewHolder> {

    public List<Atrakcija> listAtrakcija;
    public OnRecyclerItemClickListener listener;

    public interface OnRecyclerItemClickListener {
        void onRVItemClick(Atrakcija atrakcija);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView naziv;
        TextView opis;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            naziv = itemView.findViewById(R.id.tv_recycler_naziv);
            opis = itemView.findViewById(R.id.tv_recycler_opis);

        }

        public void bind(final Atrakcija objekat, final OnRecyclerItemClickListener listener) {
            naziv.setText(objekat.getNaziv());
            opis.setText(objekat.getOpis());

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemClick(objekat);
                }
            });
        }
    }

    public MojRecyclerViewAdapterListAtrakcije(List<Atrakcija> listAtrakcija, OnRecyclerItemClickListener listener) {
        this.listAtrakcija = listAtrakcija;
        this.listener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.rv_single_item, viewGroup, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder myViewHolder, int i) {
        myViewHolder.bind(listAtrakcija.get(i), listener);

    }

    @Override
    public int getItemCount() {
        return listAtrakcija.size();
    }


    public void add(Atrakcija atrakcija) {
        listAtrakcija.add(atrakcija);
        notifyDataSetChanged();
    }
}
