package com.miodrag.mikrut.turistickeatrakcije.activities;

import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.db.DatabaseHelper;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;

import java.sql.SQLException;

public class ZoomImage extends AppCompatActivity {

    DatabaseHelper databaseHelper;
    private ImageView imageView;
    private int image_path;
    private Atrakcija atrakcija;
    private int objekat_id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom_image);

        inicijalizacija();
        uzimanjeIzIntenta();

        imageView.setImageBitmap(BitmapFactory.decodeFile(atrakcija.getImage_path()));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void inicijalizacija(){
        imageView = findViewById(R.id.zoom_image);
    }

    private void uzimanjeIzIntenta(){
        objekat_id = getIntent().getIntExtra("objekat_id",1);
        try {
            atrakcija = getDatabaseHelper().getAtrakcijaDao().queryForId(objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
