package com.miodrag.mikrut.turistickeatrakcije.dialog;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;

//dialog se prikazuje pritiskom na O aplikaciji u draweru

public class AboutDialog extends AlertDialog {
    public AboutDialog(@NonNull Context context) {
        super(context);

        setTitle("O aplikaciji");
        setMessage("Miodrag Mikrut napravio za vezbu dialoga....");
        setCancelable(true);

    }
}
