package com.miodrag.mikrut.turistickeatrakcije.db.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "atrakcije")
public class Atrakcija {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField(columnName = "naziv")
    private String naziv;
    @DatabaseField(columnName = "opis")
    private String opis;
    @DatabaseField(columnName = "image_path")
    private String image_path;
    @DatabaseField(columnName = "adresa")
    private String adresa;
    @DatabaseField(columnName = "web_adresa")
    private String web_adresa;
    @DatabaseField(columnName = "telefon")
    private String telefon;
    @DatabaseField(columnName = "radno_vreme")
    private String radno_vreme;
    @DatabaseField(columnName = "cena")
    private String cena;

//    private String komentari;


    public Atrakcija() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }

    public String getAdresa() {
        return adresa;
    }

    public void setAdresa(String adresa) {
        this.adresa = adresa;
    }

    public String getWeb_adresa() {
        return web_adresa;
    }

    public void setWeb_adresa(String web_adresa) {
        this.web_adresa = web_adresa;
    }

    public String getTelefon() {
        return telefon;
    }

    public void setTelefon(String telefon) {
        this.telefon = telefon;
    }

    public String getRadno_vreme() {
        return radno_vreme;
    }

    public void setRadno_vreme(String radno_vreme) {
        this.radno_vreme = radno_vreme;
    }

    public String getCena() {
        return cena;
    }

    public void setCena(String cena) {
        this.cena = cena;
    }

    @Override
    public String toString() {
        return naziv;
    }
}
