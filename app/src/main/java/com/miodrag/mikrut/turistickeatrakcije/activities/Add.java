package com.miodrag.mikrut.turistickeatrakcije.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.db.DatabaseHelper;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;
import com.miodrag.mikrut.turistickeatrakcije.tools.Tools;

import java.sql.SQLException;

public class Add extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    private String image_path;
    DatabaseHelper databaseHelper;

    private EditText naziv;
    private EditText opis;
    private EditText telefon;
    private EditText cena;
    private EditText web_adresa;
    private EditText adresa;
    private EditText radno_vreme;
    private ImageButton imageButton;
    private Button btn_add;
    private Button btn_cancel;
//    private Slika slika;
    private Atrakcija atrakcija;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        inicijalizacija();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPicture();
            }
        });


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                add();

            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Add.this, Main.class));
            }
        });
    }

    private void inicijalizacija() {
        naziv = findViewById(R.id.add_et_naziv);
        opis = findViewById(R.id.add_et_opis);
        radno_vreme = findViewById(R.id.add_et_radno_vreme);
        adresa = findViewById(R.id.add_et_adresa);
        web_adresa = findViewById(R.id.add_et_web_adresa);
        telefon = findViewById(R.id.add_et_telefon);
        imageButton = findViewById(R.id.add_imagebutton);
        cena = findViewById(R.id.add_et_cena);
        btn_add = findViewById(R.id.add_btn_add);
        btn_cancel = findViewById(R.id.add_btn_cancel);
    }

    //dodaju se uneseni podaci u objekat i taj objekat u bazu i ispisuje notifikacije bazirano nda odabiru u Settings
    private void add() {
        //validacija unesenih polja. Provera da li su polja uneta
        if (Tools.validateInput(naziv)
                && Tools.validateInput(opis)
        ) {
            atrakcija = new Atrakcija();
            atrakcija.setNaziv(naziv.getText().toString().trim());
            atrakcija.setOpis(opis.getText().toString().trim());
            atrakcija.setTelefon(telefon.getText().toString().trim());
            atrakcija.setAdresa(adresa.getText().toString().trim());
            atrakcija.setRadno_vreme(radno_vreme.getText().toString().trim());
            atrakcija.setWeb_adresa(web_adresa.getText().toString().trim());
            atrakcija.setCena(cena.getText().toString().trim());
            atrakcija.setImage_path(image_path);
            try {
                getDatabaseHelper().getAtrakcijaDao().create(atrakcija);
                //ako je slika odabrana unosi je u bazu
//                if (image_path != null) {
//                    slika = new Slika();
//                    slika.setImage_path(image_path);
//                    slika.setNekretnina(atrakcija);
//                    getDatabaseHelper().getSlikaDao().create(slika);
//                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
            if (Tools.proveraPrefsPodesavanja("toast", Add.this)) {
                Toast.makeText(Add.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
            }
            if (Tools.proveraPrefsPodesavanja("notifications", Add.this)) {
                Tools.createNotification(this, atrakcija, "add");
            }

            Intent intent = new Intent(Add.this, Main.class);
            startActivity(intent);
            finish();
        }
    }


    /**
     * Da bi dobili pristup Galeriji slika na uredjaju
     * moramo preko URI-ja pristupiti delu baze gde su smestene
     * slike uredjaja. Njima mozemo pristupiti koristeci sistemski
     * ContentProvider i koristeci URI images/* putanju
     * <p>
     * Posto biramo sliku potrebno je da pozovemo aktivnost koja icekuje rezultat
     * Kada dobijemo rezultat nazad prikazemo sliku i dobijemo njenu tacnu putanju
     */
    //**** pocetak za odabir slike sa telefona
    //proverava dozvolu za biranje iz galerije
    public boolean isStoragePermissionGranted() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED &&
                    checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {

                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
                return false;
            }
        } else { //permission is automatically granted on sdk<23 upon installation
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
        }
    }

    //ako je dozvoljeno bira se slika iz galerije
    private void selectPicture() {
        if (isStoragePermissionGranted()) {
            Intent i = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_PICTURE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SELECT_PICTURE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            if (selectedImage != null) {
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                if (cursor != null) {
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    String picturePath = cursor.getString(columnIndex);
                    image_path = picturePath; //uzima imageButton path i stavlja ga da bude dostupno
                    cursor.close();

                    imageButton.setImageBitmap(BitmapFactory.decodeFile(image_path));
                    // String picturePath contains the path of selected Image
                }
            }
        }
    }
//*******kraj za sliku


    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
