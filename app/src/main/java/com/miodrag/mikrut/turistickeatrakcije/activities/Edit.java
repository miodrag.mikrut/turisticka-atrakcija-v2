package com.miodrag.mikrut.turistickeatrakcije.activities;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.db.DatabaseHelper;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;
import com.miodrag.mikrut.turistickeatrakcije.tools.Tools;

import java.sql.SQLException;

public class Edit extends AppCompatActivity {

    private static final int SELECT_PICTURE = 1;
    private String image_path;
    DatabaseHelper databaseHelper;

    private EditText naziv;
    private EditText opis;
    private EditText telefon;
    private EditText cena;
    private EditText web_adresa;
    private EditText adresa;
    private EditText radno_vreme;
    private ImageButton imageButton;
    private Button btn_save;
    private Button btn_cancel;
    private int objekat_id;
    //    private Slika slika;
    private Atrakcija atrakcija;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        inicijalizacija();
        preuzmiPodatkeIzIntenta();

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveChanges();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelEdit();
            }
        });
    }

    private void inicijalizacija() {
        naziv = findViewById(R.id.edit_naziv);
        opis = findViewById(R.id.edit_opis);
        radno_vreme = findViewById(R.id.edit_radno_vreme);
        adresa = findViewById(R.id.edit_adresa);
        web_adresa = findViewById(R.id.edit_web_adresa);
        telefon = findViewById(R.id.edit_telefon);
        imageButton = findViewById(R.id.edit_image_button);
        cena = findViewById(R.id.edit_cena);
        btn_save = findViewById(R.id.edit_btn_save);
        btn_cancel = findViewById(R.id.edit_btn_cancel);
    }

    private void preuzmiPodatkeIzIntenta(){
        objekat_id = getIntent().getIntExtra("objekat_id",-1);
        if(objekat_id < 0){
            Toast.makeText(this, "Greska! "+ objekat_id +" ne pstoji", Toast.LENGTH_SHORT).show();
            finish();
        }
        try {
            atrakcija = getDatabaseHelper().getAtrakcijaDao().queryForId(objekat_id);
        } catch (SQLException e) {
            e.printStackTrace();
        }


        naziv.setText(atrakcija.getNaziv());
        opis.setText(atrakcija.getOpis());
        telefon.setText(atrakcija.getTelefon());
        adresa.setText(atrakcija.getAdresa());
        web_adresa.setText(atrakcija.getWeb_adresa());
        cena.setText(atrakcija.getCena());
        radno_vreme.setText(atrakcija.getRadno_vreme());
        image_path = atrakcija.getImage_path();
        imageButton.setImageBitmap(BitmapFactory.decodeFile(image_path));

    }
    //proverava se da li je sve uneseno od polja i ako jeste setuje se objekat i upisuje u bazi
    private void saveChanges(){
        if(Tools.validateInput(naziv)
                && Tools.validateInput(opis)
                && Tools.validateInput(telefon)
                && Tools.validateInput(adresa)
                && Tools.validateInput(web_adresa)
                && Tools.validateInput(radno_vreme)
                && Tools.validateInput(cena)

        ) {
            atrakcija.setNaziv(naziv.getText().toString());
            atrakcija.setOpis(opis.getText().toString());
            atrakcija.setTelefon(telefon.getText().toString());
            atrakcija.setAdresa(adresa.getText().toString());
            atrakcija.setRadno_vreme(radno_vreme.getText().toString());
            atrakcija.setWeb_adresa(web_adresa.getText().toString());
            atrakcija.setCena(cena.getText().toString());
            atrakcija.setImage_path(image_path);


            try {
                getDatabaseHelper().getAtrakcijaDao().createOrUpdate(atrakcija);
                if(Tools.proveraPrefsPodesavanja("toast",Edit.this)) {
                    Toast.makeText(Edit.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
                }
                if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)) {
                    Tools.createNotification(this,atrakcija,"edit");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }


            Intent intent = new Intent(Edit.this, Detail.class);
            intent.putExtra("objekat_id", atrakcija.getId());
            startActivity(intent);
        }
    }

    private void cancelEdit(){
        if(Tools.proveraPrefsPodesavanja("toast",Edit.this)) {
            Toast.makeText(Edit.this, "Toast da je upisano u bazi", Toast.LENGTH_SHORT).show();
        }
        if(Tools.proveraPrefsPodesavanja("notifications",Edit.this)) {
            Tools.createNotification(this,atrakcija,"cancel");
        }
        Intent intent = new Intent(Edit.this,Detail.class);
        intent.putExtra("objekat_id",atrakcija.getId());
        startActivity(intent);
    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }
}
