package com.miodrag.mikrut.turistickeatrakcije.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;

import java.sql.SQLException;

/**
 * Created by milossimic on 11/4/16.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    //Dajemo ime bazi
    private static final String DATABASE_NAME    = "baza.db";

    //i pocetnu verziju baze. Obicno krece od 1
    private static final int DATABASE_VERSION = 1;

    private Dao<Atrakcija, Integer> atrakcijaDao = null;
//    private Dao<Slika, Integer> slikaDao = null;

    //Potrebno je dodati konstruktor zbog pravilne inicijalizacije biblioteke
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Prilikom kreiranja baze potrebno je da pozovemo odgovarajuce metode biblioteke
    //prilikom kreiranja moramo pozvati TableUtils.createTable za svaku tabelu koju imamo
    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {


//            TableUtils.createTable(connectionSource, Slika.class);
            Log.d("REZ","Kreirana brpjevi tabela u bazi");
            TableUtils.createTable(connectionSource, Atrakcija.class);
            Log.d("REZ","Kreirana kontakti tabela u bazi");

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //kada zelimo da izmenomo tabele, moramo pozvati TableUtils.dropTable za sve tabele koje imamo
    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
//            TableUtils.dropTable(connectionSource, Slika.class, true);
            TableUtils.dropTable(connectionSource, Atrakcija.class, true);
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



    //jedan Dao objekat sa kojim komuniciramo. Ukoliko imamo vise tabela
    //potrebno je napraviti Dao objekat za svaku tabelu
    public Dao<Atrakcija, Integer> getAtrakcijaDao() throws SQLException {
        if (atrakcijaDao == null) {
            atrakcijaDao = getDao(Atrakcija.class);
        }

        return atrakcijaDao;
    }
//    public Dao<Slika, Integer> getSlikaDao() throws SQLException {
//        if (slikaDao == null) {
//            slikaDao = getDao(Slika.class);
//        }
//
//        return slikaDao;
//    }

    //obavezno prilikom zatvarnaj rada sa bazom osloboditi resurse
    @Override
    public void close() {
//        slikaDao = null;
        atrakcijaDao = null;
        super.close();
    }
}
