package com.miodrag.mikrut.turistickeatrakcije.activities;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.adapters.MojRecyclerViewAdapterListAtrakcije;
import com.miodrag.mikrut.turistickeatrakcije.db.DatabaseHelper;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;
import com.miodrag.mikrut.turistickeatrakcije.dialog.AboutDialog;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Main extends AppCompatActivity implements MojRecyclerViewAdapterListAtrakcije.OnRecyclerItemClickListener  {

    private DatabaseHelper databaseHelper;

    private Toolbar toolbar;
    DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    List<String> drawerItems;
    ListView drawerList;
    RecyclerView recyclerView;
    MojRecyclerViewAdapterListAtrakcije mojRecyclerViewAdapterListAtrakcije;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();
        fillData();
        setupDrawer();
        setupRV();

    }

    private void fillData(){
        drawerItems = new ArrayList<>();
        drawerItems.add("Atrakcije");
        drawerItems.add("Settings");
        drawerItems.add("About the app");
    }

    //podesavanje Drawera
    private void setupDrawer(){
        drawerList = findViewById(R.id.left_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);
        drawerList.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, drawerItems));
        drawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String title = "Unknown";
                switch (i){
                    case 0:
                        title = "Atrakcije";

                        break;
                    case 1:
                        Toast.makeText(getBaseContext(),"Prikaz settings-a",Toast.LENGTH_SHORT);
                        title = "Settings";
                        Intent intent1 = new Intent(Main.this, SettingsActivity.class);
                        startActivity(intent1);
                        break;
                    case 2:
                        AboutDialog dialog = new AboutDialog(Main.this);
                        dialog.show();
                        title = "About the app";
                        break;
                    default:
                        break;
                }
                //drawerList.setItemChecked(i, true);
                setTitle(title);
                drawerLayout.closeDrawer(drawerList);
            }
        });

        drawerToggle = new ActionBarDrawerToggle(
                this,                           /* host Activity */
                drawerLayout,                   /* DrawerLayout object */
                toolbar,                        /* nav drawer image to replace 'Up' caret */
                R.string.app_name,           /* "open drawer" description for accessibility */
                R.string.app_name           /* "close drawer" description for accessibility */
        ) {
            public void onDrawerClosed(View view) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
//                getSupportActionBar().setTitle("");
                invalidateOptionsMenu();        // Creates call to onPrepareOptionsMenu()
            }
        };
    }

    //Pocetak setup-a za toolbar
    // action bar prikazuje opcije iz meni.xml
    //uneti u action main.xml AppBarLayout i onda Toolbar

    //setuje toolbar
    private void setupToolbar(){
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_hamburger_white);
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    // onOptionsItemSelected method is called whenever an item in the Toolbar is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create:
                Intent intent = new Intent(this, Add.class);
                startActivity(intent);
                break;
            case R.id.action_settings:
                Intent intent1 = new Intent(Main.this, SettingsActivity.class);
                startActivity(intent1);
                break;
            case R.id.action_about:
                AboutDialog dialog = new AboutDialog(this);
                dialog.show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }
    //^^^ kraj setupa za toolbar

    @Override
    public void onRVItemClick(Atrakcija objekat) {
//        Toast.makeText(this, "Kliknuto na RV listu na "+ objekat.getNaziv()+objekat.getId(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this,Detail.class);
        intent.putExtra("objekat_id",objekat.getId());

        startActivity(intent);
    }

    //stavljanj RV adatera
    private void setupRV(){
        recyclerView = findViewById(R.id.rv_container);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        try {
            mojRecyclerViewAdapterListAtrakcije = new MojRecyclerViewAdapterListAtrakcije(getDatabaseHelper().getAtrakcijaDao().queryForAll(), this);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        recyclerView.setAdapter(mojRecyclerViewAdapterListAtrakcije);

    }

    //Metoda koja komunicira sa bazom podataka
    public DatabaseHelper getDatabaseHelper() {
        if (databaseHelper == null) {
            databaseHelper = OpenHelperManager.getHelper(this, DatabaseHelper.class);
        }
        return databaseHelper;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();

        // nakon rada sa bazo podataka potrebno je obavezno
        //osloboditi resurse!
        if (databaseHelper != null) {
            OpenHelperManager.releaseHelper();
            databaseHelper = null;
        }
    }

}
