package com.miodrag.mikrut.turistickeatrakcije.tools;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.EditText;

import com.miodrag.mikrut.turistickeatrakcije.R;
import com.miodrag.mikrut.turistickeatrakcije.db.model.Atrakcija;

public class Tools {
    public static final String CHANNEL_ID = "Channel";
    public static final int notificationID = 111;

    //provera prefs settinga za checkbox notifikacije
    public static boolean proveraPrefsPodesavanja(String text, Context context){

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        boolean checkBox = prefs.getBoolean(text, true);
        return checkBox;
    }

    public static void setupNotification(Atrakcija objekat, String text, Context context, int notificationID) {
        String textForTheNotification = text;
        Notification builder = new Notification.Builder(context)
                .setContentTitle("Notification")
                .setContentText(objekat.toString() + textForTheNotification)
                .setSmallIcon(R.drawable.ic_delete)
                .build();
        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

// notificationID allows you to update the notification later on.
        mNotificationManager.notify(notificationID, builder);

    }

    //validacija unosa
    public static boolean validateInput(EditText editText) {
        String titleInput = editText.getText().toString().trim();

        if (titleInput.isEmpty()) {
            editText.setError("Polje ne moze da bude prazno");
//            editText.setError("Field can't be empty");
            return false;
        }else {
            editText.setError(null);
            return true;
        }
    }

    //klikom na broj telefona poziva broj
    public static void dialPhoneNumber(String phoneNumber,Context context) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + phoneNumber));
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    //klikom pokazuje lokaciju na mapi
    public void showMap(Uri geoLocation, Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(geoLocation);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    //otvara web lokaciju
    public static void openWebPage(String url,Context context) {
        Uri webpage = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, webpage);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
        context.startActivity(intent);
    }
    //otvara adresu na mapi
    public static void openGoogleMap(Context context, String adresa){
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                Uri.parse("google.navigation:q=an+"+adresa));
        context.startActivity(intent);
    }

    //kreiranje notifikacionog kanala i pravljenje notifikacije
    public static void createNotification(Context context, Atrakcija nekretnina, String radnja) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            int ikona = 0;
            if(radnja.equals("add")){
                ikona = R.drawable.ic_add;
            }
            if(radnja.equals("delete")){
                ikona = R.drawable.ic_delete;
            }
            if(radnja.equals("zakazi")){
                ikona = R.drawable.appointment;
            }
            if(radnja.equals("edit")){
                ikona = R.drawable.ic_edit;
            }
            if(radnja.equals("cancel")){
                ikona = R.drawable.ic_cancel;
            }
            int importance = NotificationManager.IMPORTANCE_DEFAULT;

            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Agencija nekretnina", importance);

            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
            String textForTheNotification = "Upisan u bazu";

            Notification builder = new Notification.Builder(context)
                    .setContentTitle("Notification")
                    .setContentText(nekretnina.toString() + textForTheNotification)
                    .setChannelId(CHANNEL_ID)
                    .setSmallIcon(ikona)
                    .build();
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            // notificationID allows you to update the notification later on.
            mNotificationManager.notify(notificationID, builder);
        }
    }


}
