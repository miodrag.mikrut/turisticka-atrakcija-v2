package com.miodrag.mikrut.turistickeatrakcije.dialog;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;


public class DeleteDialog extends AlertDialog.Builder {
    public DeleteDialog(@NonNull Context context) {
        super(context);

        setTitle("Are you sure want to delete?");
        setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
    }
}
